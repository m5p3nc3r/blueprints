# Autoware Open AD Kit Blueprint
## Introduction 
[Open AD Kit](https://www.autoware.org/autoware-open-ad-kit) is developed by the [Autoware Foundation](https://www.autoware.org/) (AWF) and work is done in the Open AD Kit [Work Group](https://www.autoware.org/join-a-work-group). The project aims to bring software defined best practices to the open-source autonomous drive software stack [Autoware](https://github.com/autowarefoundation), e.g., by introducing cloud-native development and production methodologies. The Open AD Kit deployment model for Autoware is a microservices architecture with broad support for the cloud, at the edge and in virtual hardware.

The Autoware Open AD Kit Blueprint takes the output application workload(s) from the Open AD Kit Work Group and deploys it in various ways, showing with practical examples how the SOAFEE vision of software defined vehicles can be materialized.

## Structure
<img src="./images/oak_bp_overview.jpg" alt="oak_bp_overview" width="600"/>

## Releases
| Release Date | Release |
| ------------ | ------- |
| 2022 October | [R1](r1/README.md) |